require 'test_helper'

class VenuesControllerTest < ActionDispatch::IntegrationTest
  test "venues page" do
    post login_url, params: { username: 'one', password: 'MyText' }

    get venues_url
    assert_response :success
  end

  test "venue page" do
    post login_url, params: { username: 'one', password: 'MyText' }

    get venue_url(venues(:meeting_room_1).id)
    assert_response :success
  end

end
