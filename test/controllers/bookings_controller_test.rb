require 'test_helper'

class BookingsControllerTest < ActionDispatch::IntegrationTest
  test "new booking page" do
    post login_url, params: { username: 'one', password: 'MyText' }

    get new_venue_booking_url(venues(:meeting_room_1).id)
    assert_response :success
  end

  test "should create booking" do
    post login_url, params: { username: 'one', password: 'MyText' }

    assert_difference('Booking.count') do
      post venue_bookings_url(venues(:meeting_room_1).id), 
      params: { booking: { from: '2020-10-22 09:00:00', to: '2020-10-22 10:00:00' } }
    end
    
    assert_response :redirect
    assert_redirected_to controller: "venues", action: "show", id: venues(:meeting_room_1).id
  end

  test "should delete booking" do
    booking = bookings(:one)

    post login_url, params: { username: 'one', password: 'MyText' }

    assert_difference('Booking.count', -1) do
      delete venue_booking_url(booking.venue_id, booking.id)
    end
    
    assert_response :redirect
    assert_redirected_to controller: "venues", action: "show", id: venues(:meeting_room_1).id
  end

  test "cannot delete someone else's booking" do
    booking = bookings(:two)

    post login_url, params: { username: 'one', password: 'MyText' }

    assert_difference('Booking.count', 0) do
      delete venue_booking_url(booking.venue_id, booking.id)
    end
    
    assert_response :redirect
    assert_redirected_to controller: "venues", action: "show", id: venues(:meeting_room_1).id

    assert_equal "You do not own this booking.", flash[:danger]
  end

end
