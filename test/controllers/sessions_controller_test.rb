require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test "login page" do
    get login_url
    assert_response :success
  end

  test "login success" do
    post login_url, params: { username: 'one', password: 'MyText' }
    assert_response :redirect
    assert_redirected_to venues_url

    assert logged_in?
    assert current_user.id == users(:one).id
  end

  test "incorrect password" do
    post login_url, params: { username: 'one', password: 'xxx' }
    assert_response :redirect
    assert_redirected_to login_url

    assert_not logged_in?
    
    assert_equal "Invalid email/password combination", flash[:danger]
  end

  test "get /venues with no auth" do
    get venues_url
    assert_response :redirect
    assert_redirected_to controller: "sessions", action: "new"
  end

  test "get /venues with auth" do
    post login_url, params: { username: 'one', password: 'MyText' }

    get venues_url
    assert_response :success
  end

  test "logout page" do
    get logout_url
    assert_response :found
  end

  test "logout should work" do
    post login_url, params: { username: 'one', password: 'MyText' }

    get venues_url
    assert_response :success

    get logout_url
    assert_response :found

    get venues_url
    assert_response :redirect
    assert_redirected_to controller: "sessions", action: "new"
  end

end
