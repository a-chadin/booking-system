class VenuesController < ApplicationController
  before_action :logged_in_user

  def index
    @venues = Venue.all
  end

  def show
    @venue = Venue.find(params[:id])
  end
end
