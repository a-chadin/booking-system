class BookingsController < ApplicationController
  before_action :logged_in_user

  def new
    @venue = Venue.find(params[:venue_id])
  end

  def create
    @venue = Venue.find(params[:venue_id])
    @booking = @venue.bookings.new(booking_params)
    user = current_user
    @booking.user = user
    @booking.save
    redirect_to venue_path(@venue)
  end

  def edit
  end

  def update
  end

  def destroy
      @venue = Venue.find(params[:venue_id])
    @booking = Booking.find(params[:id])

    if @booking.user != current_user
      flash[:danger] = "You do not own this booking."
      else
    @booking.destroy
      end

      redirect_to venue_path(@venue)
  end

  private
    def booking_params
      params.require(:booking).permit(:from, :to)
    end
end
